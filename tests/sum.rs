mod common;
use common::{set_up, teardown};

use master_rust::chapter3::sum;

#[test]
fn sum_test() {
    assert_eq!(sum(2, 3), 5);
}

#[test]
fn test_with_fixture() {
    set_up();
    assert_eq!(sum(4, 3), 7);
    teardown();
}
