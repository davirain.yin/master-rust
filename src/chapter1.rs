pub mod greet {
    use env_logger;
    use std::env;

    // Returns the arguments that this program was started with (normally passed via the command line).
    // The first element is traditionally the path of the executable, but it can be set to arbitrary text, and may not even exist. This means this property should not be relied upon for security purposes.
    // On Unix systems the shell usually expands unquoted arguments with glob patterns (such as * and ?). On Windows this is not done, and such arguments are passed as-is.
    // On glibc Linux systems, arguments are retrieved by placing a function in .init_array. glibc passes argc, argv, and envp to functions in .init_array, as a non-standard extension. This allows std::env::args to work even in a cdylib or staticlib, as it does on macOS and Windows.
    // Panics
    // The returned iterator will panic during iteration if any argument to the process is not valid Unicode. If this is not desired, use the [args_os] function instead.
    // fn args() -> Args;

    /// parse  command, MUST supply one args.
    pub fn greet() {
        env_logger::init();

        let name = env::args().nth(1);
        let result = name.map_or_else(
            || panic!("Didn't receive any name"),
            |n| {
                log::info!("Hi there ! {}", n);
                n.parse::<i32>()
            },
        );
        let ret = result.map_or_else(
            |_| {
                log::info!("default value");
                23
            },
            |value| {
                log::info!("real value : {}", value);
                value
            },
        );

        log::info!("ret = {}", ret);
        // match name {
        //     Some(n) => println!("Hi there ! {}", n),
        //     None => panic!("Didn't receive any name."),
        // }
    }

    #[test]
    fn test_greet() {
        // TODO
        // greet();
    }

    /// Creates an iterator that skips the first n elements.
    /// skip(n) skips elements until n elements are skipped or the end of the iterator is reached (whichever happens first). After that, all the remaining elements are yielded. In particular, if the original iterator is too short, then the returned iterator is empty.
    /// Rather than overriding this method directly, instead override the nth method
    /// fn skip(self, n: usize) -> Skip<Self> where     Self: Sized,
    #[test]
    fn test_iterator_skip() {
        let array = [1, 2, 3];

        let skip = array.iter().skip(4).next();

        assert_eq!(skip, None);

        let array: Vec<i32> = vec![];

        let skip = array.iter().skip(1).next();

        assert_eq!(skip, None);
    }
}

/// error[E0384]: cannot assign twice to immutable variable `target`
pub mod variables {

    /// # E0384
    /// An immutable variable was reassigned.
    /// Erroneous code example:
    /// ```no_run
    /// fn main() {
    ///     let x = 3;
    ///     x = 5; // error, reassignment of immutable variable
    /// }
    /// ```
    /// By default, variables in Rust are immutable.
    /// To fix this error, add the keyword mut after the keyword let when declaring the variable. For example:
    ///
    /// ```no_run
    /// fn main() {
    ///     let mut x = 3;
    ///     x = 5;
    /// }
    /// ```
    #[test]
    fn test_immutable_and_mutable() {
        let target = "world";
        let mut greeting = "Hello";
        println!("{}, {}", greeting, target);

        greeting = "How are you doing";

        // target = "mate"; // Cannot assign twice to immutable variable [E0384]

        println!("{}, {}", greeting, target);
    }
}

pub mod function {
    pub fn add(a: u64, b: u64) -> u64 {
        a + b
    }

    /// increase val to how_much, but this func don't increase This self val
    pub fn increase_by_val(mut val: u32, how_much: u32) {
        val += how_much;

        println!("You made {} points", val);
    }

    /// increase val to how_much, This increase this self val
    pub fn increase_by_ref(val: &mut u32, how_much: u32) {
        *val += how_much;

        println!("You made {} points", val);
    }

    #[test]
    fn test_add_func() {
        assert_eq!(1, add(0, 1));
    }

    #[test]
    fn test_increase_by_val_func() {
        let score = 2048;
        assert_eq!(score, 2048);
        increase_by_val(score, 30);
        assert_ne!(score, 2078);
    }

    #[test]
    fn test_increase_by_ref_func() {
        let mut score = 2048;
        assert_eq!(score, 2048);
        increase_by_ref(&mut score, 30);
        assert_eq!(score, 2078);
    }
}

pub mod closures {

    #[test]
    fn test_closure() {
        // test double closure
        let doubler = |x| x * 2;
        let value = 5;
        let twice = doubler(value);
        assert_eq!(twice, 10);

        // test big_closure
        let big_closure = |b, c| {
            let z = b + c;
            z * twice
        };

        let some_number = big_closure(1, 2);
        assert_eq!(some_number, 30);
    }
}

pub mod string {

    #[test]
    fn test_string() {
        let question = "How are you ?";
        let person = "Bob".to_string();

        let han_zi = String::from("👋 世界！");

        println!("{}, {}, {}", question, person, han_zi);
    }
}

pub mod if_else {

    #[test]
    fn test_if_else() {
        let rust_is_awsome = true;
        if rust_is_awsome {
            println!("Indeed");
        } else {
            println!("Well, you should try Rust!");
        }
    }

    #[test]
    fn test_if_assign() {
        let result = if 1 == 2 {
            "wait, What ?"
        } else {
            "Rust makes sense"
        };

        assert_eq!(result, "Rust makes sense");
        println!("You know what ? {}.", result);
    }

    #[test]
    fn test_if_no_value() {
        let result = if 1 == 2 {
            "Nothing makes sense";
        } else {
            "Sanity reigns";
        };

        assert_eq!(result, ());

        println!("Result of computation: {:?}", result);
    }
}

pub mod match_expression {

    fn req_states() -> u32 {
        200
    }

    #[test]
    fn test_match_expression() {
        let status = req_states();
        match status {
            200 => println!("Success"),
            404 => println!("Not Found"),
            other => println!("Request failed with code: {}", other),
        }
    }
}

pub mod scope_loop {

    #[test]
    fn test_loop() {
        let mut x = 1024;
        loop {
            if x <= 0 {
                break;
            }
            println!("{} more runs to go", x);
            x -= 1;
        }
        assert_eq!(x, 0);
    }

    fn silly_sub(a: i32, b: i32) -> i32 {
        let mut result = 0;
        'increment: loop {
            if result == a {
                let mut dec = b;
                'decrement: loop {
                    if dec == 0 {
                        break 'increment;
                    } else {
                        result -= 1;
                        dec -= 1;
                    }
                }
            } else {
                result += 1;
            }
        }
        result
    }

    #[test]
    fn test_loop_lables() {
        assert_eq!(6, silly_sub(10, 4));
    }

    #[test]
    fn test_while() {
        let mut x = 1000;
        while x > 0 {
            println!("{} more runs to go", x);
            x -= 1;
        }
        assert_eq!(x, 0);
    }

    #[test]
    fn test_for() {
        println!("Normal ranges: ");
        for i in 0..10 {
            print!("{}, ", i);
        }

        println!();
        println!("Inclusive ranges: ");
        for i in 0..=10 {
            print!("{}, ", i);
        }
    }
}

pub mod self_define_struct {
    use std::fmt;
    use std::fmt::Display;

    /// unit struct
    #[derive(Debug)]
    struct Dummy;

    impl fmt::Display for Dummy {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "Dummy")
        }
    }

    /// tuple struct
    #[derive(Debug)]
    struct Color(u8, u8, u8);

    impl fmt::Display for Color {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "(red: {}, green: {}, blue: {})", self.0, self.1, self.2)
        }
    }

    impl Color {
        fn get_value(&self) -> (u8, u8, u8) {
            (self.0, self.1, self.2)
        }
    }

    /// struct
    #[derive(Debug)]
    struct Player {
        name: String,
        iq: u8,
        friends: u8,
        score: u16,
    }

    impl fmt::Display for Player {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            writeln!(f, "Name: {}", self.name)?;
            writeln!(f, "IQ: {}", self.iq)?;
            writeln!(f, "Friends: {}", self.friends)?;
            writeln!(f, "Score: {}", self.score)
        }
    }

    impl Player {
        fn new() -> Self {
            Self {
                name: String::new(),
                iq: u8::default(),
                friends: u8::default(),
                score: u16::default(),
            }
        }

        fn alice_account() -> Self {
            Self {
                name: "Alice".to_string(),
                iq: 100,
                friends: 124,
                score: 1129,
            }
        }

        fn bump_palyer_score(&mut self, score: u16) {
            self.score += score;
            // println!("Update palyer stats:");
            // println!("Name: {}", self.name);
            // println!("IQ: {}", self.iq);
            // println!("Friends: {}", self.friends);
            // println!("Score: {}", self.score);
        }
    }

    /// enum
    #[derive(Debug)]
    enum Direction {
        N,
        E,
        S,
        W,
    }

    #[derive(Debug)]
    enum PlayerAction {
        Move { direction: Direction, speed: u8 },
        Wait,
        Attack(Direction),
    }

    #[test]
    fn test_enum() {
        let simulated_palyer_action = PlayerAction::Move {
            direction: Direction::N,
            speed: 2,
        };
        match simulated_palyer_action {
            PlayerAction::Wait => println!("Player wants to wait"),
            PlayerAction::Move { direction, speed } => {
                println!(
                    "Player wants move in direction {:?} with speed {}",
                    direction, speed
                );
            }
            PlayerAction::Attack(direction) => {
                println!("Player wants to attack direction {:?}", direction);
            }
        }
    }

    #[test]
    fn test_unit_struct() {
        let value = Dummy;
        // println!("{}", value);
        println!("{:?}", value);
    }

    #[test]
    fn test_tuple_struct() {
        let white = Color(255, 255, 255);
        let (_r, _g, _b) = white.get_value();
        println!("white = {}", white);
        println!("white = {:?}", white);
    }

    #[test]
    fn test_struct() {
        let mut player = Player::new();
        let mut player = Player::alice_account();
        println!("player = {}", player);
        println!("player = {:?}", player);
        player.bump_palyer_score(212);
        println!("player = {}", player);
    }
}

pub mod struct_methods {
    use std::fmt;

    /// struct
    #[derive(Debug)]
    struct Player {
        name: String,
        iq: u8,
        friends: u8,
        score: u16,
    }

    impl fmt::Display for Player {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            writeln!(f, "Name: {}", self.name)?;
            writeln!(f, "IQ: {}", self.iq)?;
            writeln!(f, "Friends: {}", self.friends)?;
            writeln!(f, "Score: {}", self.score)
        }
    }

    impl Player {
        fn new() -> Self {
            Self {
                name: String::new(),
                iq: u8::default(),
                friends: u8::default(),
                score: u16::default(),
            }
        }

        fn with_name(name: &str) -> Self {
            Self {
                name: name.to_string(),
                iq: 100,
                friends: 100,
                score: 100,
            }
        }

        fn get_friend(&self) -> u8 {
            self.friends
        }

        fn set_friend(&mut self, count: u8) {
            self.friends = count;
        }

        fn alice_account() -> Self {
            Self {
                name: "Alice".to_string(),
                iq: 100,
                friends: 124,
                score: 1129,
            }
        }

        fn bump_palyer_score(&mut self, score: u16) {
            self.score += score;
        }
    }

    #[test]
    fn test_struct_metohds() {
        let mut player = Player::with_name("Alice");
        player.set_friend(23);
        println!("{}", player);

        let ret = Player::get_friend(&player);
        println!("firend: {}", ret);
    }
}

pub mod enum_methods {

    #[derive(Debug)]
    enum PaymentMode {
        Debit,
        Credit,
        Paypal,
    }

    fn pay_by_credit(amt: u64) {
        println!("Processing credit payment of {}", amt);
    }

    fn pay_by_debit(amt: u64) {
        println!("Processing debit payment of {}", amt);
    }

    fn paypal_redirect(amt: u64) {
        println!("Redirecting to paypal for amount: {}", amt);
    }

    impl PaymentMode {
        fn pay(&self, amount: u64) {
            match self {
                PaymentMode::Debit => pay_by_debit(amount),
                PaymentMode::Credit => pay_by_credit(amount),
                PaymentMode::Paypal => paypal_redirect(amount),
            }
        }
        fn get_saved_payment_mode() -> PaymentMode {
            PaymentMode::Debit
        }
    }

    #[test]
    fn test_enum_methods() {
        let payment_mode = PaymentMode::get_saved_payment_mode();
        payment_mode.pay(23);
    }
}

pub mod collects {

    #[test]
    fn test_arrays() {
        let numbers: [u8; 10] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        let floats = [0.1f64, 0.2, 0.3];

        println!("Number: {}", numbers[5]);
        println!("Float: {}", floats[2]);
    }

    #[test]
    fn test_tuples() {
        let num_and_str: (u8, &str) = (40, "Have a good day!");
        println!("{:?}", num_and_str);

        let (num, string) = num_and_str;
        println!("From tuple: Number: {}, String: {}", num, string);
    }

    #[test]
    fn test_vec() {
        let mut numbers_vec: Vec<u8> = Vec::new();
        numbers_vec.push(1);
        numbers_vec.push(2);

        let mut vec_with_macro = vec![1];
        vec_with_macro.push(2);
        let _ = vec_with_macro.pop();

        let message = if numbers_vec == vec_with_macro {
            "The are equal"
        } else {
            "Nah! They look different to me"
        };

        println!("{} {:?} {:?}", message, numbers_vec, vec_with_macro);
    }

    #[test]
    fn test_hashmap() {
        use std::collections::HashMap;

        let mut fruits = HashMap::new();
        fruits.insert("apple", 3);
        fruits.insert("mango", 6);
        fruits.insert("orange", 2);
        fruits.insert("avocado", 7);

        for (k, v) in fruits.iter() {
            println!("I got {} {}", k, v);
        }

        fruits.remove("orage");
        let old_avocado = fruits["avocado"];
        fruits.insert("avocado", old_avocado + 5);
        println!("\nI now have {} avocado", fruits["avocado"]);
    }

    #[test]
    fn test_slices() {
        let mut numbers: [u8; 4] = [1, 2, 3, 4];
        {
            let all: &[u8] = &numbers[..];
            println!("All of them: {:?}", all);
        }

        {
            let first_two: &mut [u8] = &mut numbers[0..2];
            first_two[0] = 100;
            first_two[1] = 99;
            println!("Look ma! I can modify through slice: {:?}", numbers);
        }
    }
}
