/// Sum two arguments
///
/// # Example
///
/// ```
/// use master_rust::chapter3::sum;
/// assert_eq!(sum(1, 1), 2);
/// ```
pub fn sum(a: i8, b: i8) -> i8 {
    a + b
}

fn silly_loop() {
    for _ in 1..1_000_000_000 {}
}

#[cfg(test)]
mod tests {

    fn sum_inputs_outputs() -> Vec<((i8, i8), i8)> {
        vec![((1, 1), 2), ((0, 0), 0), ((2, -2), 0)]
    }

    #[test]
    fn test_sums() {
        for (input, output) in sum_inputs_outputs() {
            assert_eq!(super::sum(input.0, input.1), output);
        }
    }

    #[test]
    fn basic_test() {
        assert!(true);
    }

    // 故障测试
    #[test]
    #[should_panic]
    fn this_panic() {
        assert_eq!(1, 2);
    }

    // 忽略测试
    #[test]
    #[ignore]
    fn test_silly_loop() {
        super::silly_loop()
    }
}
