// 创建泛型

// 泛型函数
fn give_me<T>(value: T) {
    let _ = value;
}

// 泛型结构体
struct GenericsStruct<T>(T);

struct Container<T> {
    item: T,
}

#[test]
fn test_give_me(){
    let a = "generics";
    let b = 1023;
    give_me(a);
    give_me(b);
}