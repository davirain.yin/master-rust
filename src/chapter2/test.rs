#[test]
fn test_food_mod() {
    use super::food::Cake;

    let eatable = Cake;
    println!("{:?}", eatable);
}

#[test]
fn minus_two_raised_three_is_minus_eight() {
    use super::pow::pow;
    assert_eq!(pow(-2, 3), -8);
}
