#![allow(unused_imports)]

use master_rust::chapter1::greet;
use master_rust::chapter2;
use master_rust::foo;

fn main() {
    let _ = chapter2::foo::Bar::init();
    let _ = chapter2::bar::Bar::hello();
}
