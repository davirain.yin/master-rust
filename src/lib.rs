#![allow(unused_imports)]
#![allow(unused_labels)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unused_mut)]

//! This crate provides functionality for adding things
//!
//! # Example
//! ```
//! use master_rust::chapter3::sum;
//!
//! let work_a = 5;
//! let work_b = 34;
//!
//! let total_work = sum(work_a, work_b);
//! assert_eq!(total_work, 39);
//! ```

#![feature(test)]
extern crate test;

use test::Bencher;

pub fn do_nothing_slowly() {
    print!(".");
    for _ in 1..10_000_000 {}
}

pub fn do_noting_fast() {}

#[bench]
fn bench_nothing_slowly(b: &mut Bencher) {
    b.iter(|| do_nothing_slowly());
}

#[bench]
fn bech_noting_fast(b: &mut Bencher) {
    b.iter(|| do_noting_fast())
}

pub mod chapter1;
pub mod chapter2;
pub mod chapter3;
pub mod chapter4;

pub mod foo {
    pub fn baz() {
        println!("baz");
    }

    pub fn zoo() {
        println!("zoo");
    }
}
