use std::collections::HashMap;
use std::env;
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::io::prelude::{BufRead, Write};
use std::io::{BufReader, BufWriter};

#[derive(Debug)]
struct WordCounter(HashMap<String, u64>);

impl WordCounter {
    fn new() -> Self {
        Self(HashMap::new())
    }

    fn increment(&mut self, word: &str) {
        let key = word.to_string();
        let count = self.0.entry(key).or_insert(0);
        *count += 1;
    }

    fn display(&self, filter: u64) {
        let mut result = self.to_vec();

        result.sort_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap());

        for (key, value) in result.iter() {
            if value > &filter {
                println!("{}: {}", key, value);
            }
        }
    }

    fn to_vec(&self) -> Vec<(String, u64)> {
        let mut result = vec![];
        for val in self.0.iter() {
            result.push((val.0.clone(), *val.1));
        }
        result
    }
}

impl Display for WordCounter {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for (key, value) in self.0.iter() {
            writeln!(f, "{}: {}", key, value)?;
        }
        write!(f, "")
    }
}

fn main() {
    let arguments: Vec<String> = env::args().collect();
    let input_filename = &arguments[1];
    let output_filename = &arguments[2];

    println!("Processing input file: {}", input_filename);

    let file = File::open(input_filename).expect("Could not open file");
    let reader = BufReader::new(file);
    let mut word_counter = WordCounter::new();

    for line in reader.lines() {
        let line = line.expect("Could not read line");
        let words = line.split(' ');
        for word in words {
            if word.is_empty() {
                continue;
            } else {
                word_counter.increment(word);
            }
        }
    }

    println!("Processing output filename: {}", output_filename);

    let out_file = File::create(output_filename).expect("Could not create file");

    let mut writer = BufWriter::new(out_file);

    let temp = format!("{}", word_counter);

    writer.write_all(temp.as_bytes()).unwrap();

    word_counter.display(0);
    // println!("{}",word_counter);
}
