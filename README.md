# Rust 学习资料

## Master Rust

- Chapter 1 Rust入门
- Chapter 2 使用Cargo管理项目
- Chapter 3 测试、文档化和基准测试
- Chapter 4 类型、泛型和特征
- Chapter 5 内存管理和安全性
- Chapter 6 异常处理
- Chapter 7 高级概念
- Chapter 8 并发
- Chapter 9 宏与元编程
- Chapter 10 不安全的Rust和外部函数
- Chapter 11 日志
- Chapter 12 Rust与网络编程
- Chapter 13 用Rust构建Web应用
- Chapter 14 Rust与数据库
- Chapter 15 Rust与WebAssembly
- Chapter 16 Rust与桌面应用
- Chapter 17 调试

## Master Rust 阅读笔记

在较高的层面，Rust被组织成模块的形式，根模块会包含一个main()
函数。对于二进制可执行项目，根模块通常是一个main.rs文件，
而对于程序库，根模块通常是一个lib.rs文件。

