use master_rust::chapter2::pow::pow;

fn main() {
    println!("8 raised to 2 is {}", pow(8, 2));
}
